package pcd.ass03.ex3;

import io.reactivex.Flowable;
import pcd.ass03.ex3.core.ObservableTemperatureSensor;

/**
 * Created by Xander_C on 14/05/2017.
 */
public class BRoom implements Room {

    private final String name;
    private ObservableTemperatureSensor sensor;
    private final Flowable<Double> hotTemps;
    private final Flowable<Double> cleanTemps;

    public BRoom(final String name, final ObservableTemperatureSensor sensor) {
        this.name = name;
        this.sensor = sensor;
        this.hotTemps = sensor.createObservable().publish().autoConnect();
        this.cleanTemps = hotTemps.filter(d -> d <= 1000.0);
    }

    @Override
    public String name() {
        return name;
    }

    public Flowable<Double> peek() {
        return cleanTemps;
    }
}
