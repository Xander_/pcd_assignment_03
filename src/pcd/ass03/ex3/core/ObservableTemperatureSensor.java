package pcd.ass03.ex3.core;

import io.reactivex.Flowable;

public interface ObservableTemperatureSensor extends TemperatureSensor {
	
	Flowable<Double> createObservable();
	
}
