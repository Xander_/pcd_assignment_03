package pcd.ass03.ex3;

import javax.swing.*;
import java.awt.*;

/**
 *
 * Created by Xander_C on 14/05/2017.
 */
public class TempView extends JFrame {

    public TempView(final String title, final HouseMonitor controller,
                    final int w, final int h){
        this.setSize(w, h);
        this.setTitle(title);
        this.setLayout(new BorderLayout(10, 10));

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        final JPanel north = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
//        final LayoutManager lm = new BoxLayout(north, BoxLayout.Y_AXIS);
//        north.setLayout(lm);
        final JLabel infoA1 = WriteOnlyBuffer.instance().info;

        north.add(infoA1);

        final JPanel main = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        final JTextArea error = WriteOnlyBuffer.instance().error;
        final JScrollPane scrollPane= new JScrollPane(error);
        scrollPane.setVisible(false);
        scrollPane.createVerticalScrollBar();
        main.add(scrollPane);

        final JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        final JButton start = new JButton("Start");

        start.addActionListener(e -> {
            scrollPane.setVisible(true);
            controller.start(WriteOnlyBuffer.instance());
            start.setEnabled(false);
        });

        south.add(start);

        this.add(north, BorderLayout.NORTH);
        this.add(main, BorderLayout.CENTER);
        this.add(south, BorderLayout.SOUTH);

        this.setResizable(false);
        this.setVisible(true);
    }

    public static class WriteOnlyBuffer{

        private static final WriteOnlyBuffer SINGLETON = new WriteOnlyBuffer();

        private final JLabel info;
        private final JTextArea error;
        private final int errorHistorySize = 50;
        private int actual = 0;

        private WriteOnlyBuffer(){
            this.info = new JLabel("[...@...°C][...@...°C][...@...°C]");
            this.error = new JTextArea();
            error.setAutoscrolls(true);
            error.setEditable(false);
            error.setRows(errorHistorySize/5);
            error.setColumns(45);
        }

        public static WriteOnlyBuffer instance(){
            return WriteOnlyBuffer.SINGLETON;
        }

        public void info(final String s) {
            info.setText(s);
        }

        public void error(final String e) {
            error.append(e + "\n");
        }
    }
}
