package pcd.ass03.ex3;

import io.reactivex.Flowable;
import javafx.util.Pair;
import pcd.ass03.ex3.acme.TemperatureSensorA1;
import pcd.ass03.ex3.acme.TemperatureSensorB1;
import pcd.ass03.ex3.acme.TemperatureSensorB2;

import javax.swing.*;
import java.time.LocalDateTime;

/**
 *
 * Created by Xander_C on 14/05/2017.
 */
public class HouseMonitor {

    private final Room A1;
    private final Room B1;
    private final Room B2;

    public HouseMonitor() {
        A1 = new ARoom("A-1", new TemperatureSensorA1());
        B1 = new BRoom("B-1", new TemperatureSensorB1());
        B2 = new BRoom("B-2", new TemperatureSensorB2());
    }

    public void start(final TempView.WriteOnlyBuffer b){

        Flowable.combineLatest(A1.peek(), B1.peek(), B2.peek(),
                (a1, b1, b2) -> new Pair<>("[" +
                        A1.name() + "@" + a1.toString() + "][" +
                        B1.name() + "@" + b1.toString() + "][" +
                        B2.name() + "@" + b2.toString() + "]",
                        b1 > 15.0 && b2 > 15.0)
            ).subscribe(p -> SwingUtilities.invokeLater(() -> {
                b.info(p.getKey());

                if (p.getValue()){
                    b.error("> from [System] @ [" + LocalDateTime.now().toString() +"]: " +
                                "Temp out of bound on both Rooms B1 and B2");
                }
            })
        );
    }
}
