package pcd.ass03.ex3;

import io.reactivex.Flowable;

/**
 * Created by Xander_C on 14/05/2017.
 */
public interface Room {

    String name();

    Flowable<Double> peek();
}
