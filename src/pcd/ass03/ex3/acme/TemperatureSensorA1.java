package pcd.ass03.ex3.acme;

import pcd.ass03.ex3.core.TemperatureSensor;

/**
 * Created by Xander_C on 14/05/2017.
 */
public class TemperatureSensorA1 extends TempSensorImpl implements TemperatureSensor {
    public TemperatureSensorA1() {
        super(5.0D, 15.0D, 1.0D, 0.05D);
    }
}

