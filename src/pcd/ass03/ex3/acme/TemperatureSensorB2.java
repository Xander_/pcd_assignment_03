package pcd.ass03.ex3.acme;


import io.reactivex.Flowable;
import pcd.ass03.ex3.core.ObservableTemperatureSensor;

public class TemperatureSensorB2 extends ObservableTempSensorImpl implements ObservableTemperatureSensor {
    public TemperatureSensorB2() {
        super(10.0D, 20.0D, 1.0D, 0.05D);
    }

    public Flowable<Double> createObservable() {
        return this.createObservable(500);
    }
}
