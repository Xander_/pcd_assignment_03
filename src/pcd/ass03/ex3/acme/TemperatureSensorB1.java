package pcd.ass03.ex3.acme;

import io.reactivex.Flowable;
import pcd.ass03.ex3.core.ObservableTemperatureSensor;

public class TemperatureSensorB1 extends ObservableTempSensorImpl implements ObservableTemperatureSensor {
    public TemperatureSensorB1() {
        super(0.0D, 10.0D, 1.8D, 0.1D);
    }

    public Flowable<Double> createObservable() {
        return this.createObservable(100);
    }
}