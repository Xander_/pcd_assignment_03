package pcd.ass03.ex3;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import pcd.ass03.ex3.core.TemperatureSensor;

/**
 * Created by Xander_C on 14/05/2017.
 */
public class ARoom implements Room {

    private final String name;

    private TemperatureSensor sensor;
    private boolean isSensorUp = true;

    private final Flowable<Double> hotTemps;
    private final Flowable<Double> cleanTemps;

    public ARoom(final String name, final TemperatureSensor sensor) {

        this.sensor = sensor;
        this.name = name;

        final Flowable<Double>temps = Flowable.create(e -> {
            new Thread(() -> {

                while (isSensorUp) {
                    try {
                        e.onNext(sensor.getCurrentValue());
                        Thread.sleep(200L);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }).start();
        }, BackpressureStrategy.BUFFER);

         hotTemps = temps.publish().autoConnect();
         cleanTemps = hotTemps.filter(d -> d <= 1000.0);
    }

    @Override
    public String name() {
        return name;
    }

    public Flowable<Double> peek() {
        return cleanTemps;
    }
}
