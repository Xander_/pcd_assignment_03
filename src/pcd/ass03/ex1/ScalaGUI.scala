package pcd.ass03.ex1

import akka.actor.ActorRef
import javax.swing._
import java.awt._
import java.awt.event.ActionEvent
import java.lang.Integer._

/**
  * A simple class to manage a GUI
  *
  * Created by Alessandro on 07/05/2017.
  */
class ScalaGUI(private val updater: ActorRef) extends JFrame {
    
    final private val turn = new JLabel("")
    final private val winner = new JLabel("")
    final private val magic = new JLabel("")
    
    def show(title: String, w: Int, h: Int) : Unit = {
        
        this.setSize(w, h)
        this.setTitle(title)
        this.setLayout(new BorderLayout(10, 10))
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
        
        val north = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10))
        val tickets = new JLabel("Tickets")
        val spinner = new JSpinner(new SpinnerNumberModel(3, 1, MAX_VALUE, 1))
        north.add(tickets)
        north.add(spinner)
        
        val main = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10))
        
        val start = new JButton("Start")
        val reset = new JButton("Reset")
        val exit = new JButton("Exit")
        
        start.addActionListener((e: ActionEvent) => {
                updater.tell(Message.Start(spinner.getValue.asInstanceOf[Int]), null)
                start.setEnabled(false)
                reset.setEnabled(true)
            })
        reset.setEnabled(false)
        reset.addActionListener(_ => {
                updater.tell(Message.Reset(), null)
                start.setEnabled(true)
                reset.setEnabled(false)
            })
        
        exit.addActionListener(_ => updater.tell(Message.Close(), null))
        main.add(start)
        main.add(reset)
        main.add(exit)
        
        val south = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10))
        south.add(magic)
        south.add(turn)
        south.add(winner)
        
        this.add(north, BorderLayout.NORTH)
        this.add(main, BorderLayout.CENTER)
        this.add(south, BorderLayout.SOUTH)
    }
    
    def setNumberDisplay(value: String) = this.magic.setText(value)
    
    def setWinnerDisplay(value: String) = this.winner.setText(value)
    
    def setTurnDisplay(value: String) = this.turn.setText(value)
}