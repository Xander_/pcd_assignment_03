package pcd.ass03.ex1

import pcd.ass03.ex1.TicketDispenser.DEFAULT_STARTING_TICKET

import scala.collection.immutable

/**
  * Created by Alessandro on 07/05/2017.
  */

class TicketDispenser extends BasicActor{
    
    val DEFAULT_BUYER = "None"
    private var maxAvailableTickets = 0
    private var ticketMap = immutable.Map.empty[Int, String]
    private var turn : Int = _
    
    protected def init(args: List[String]) = args match{
        case nTickets :: _ =>
            this.maxAvailableTickets = nTickets.toInt
            this.log(this.ticketMap.size.toString + " tickets are available each turn...")
            
        case _ => throw new IllegalArgumentException("Illegals Init Arguments for " + this.getClass)
    }
    
    override protected val receptive = {
        case msg : Message.TicketRequest =>
            if (ticketMap.exists(e => e._2 == msg.sender)){
                this.log(msg.sender + " <= You have already purchased your ticket for turn " + turn +"...")
                this.sender.tell(Message.TicketResponse(-1), self)
            } else {
                val temp = ticketMap.filter(e => e._2 == DEFAULT_BUYER)
            
                if (temp.nonEmpty){
                    val (ticketValue, _) = temp.minBy(e => e._1)
                    this.log("Delivering ticket #" + ticketValue +" to " + msg.sender)
                    ticketMap = ticketMap.updated(ticketValue, msg.sender)
                    this.sender ! Message.TicketResponse(ticketValue)
                
                } else {
                    this.log(msg.sender + " <= No tickets are available at the moment...")
                    this.sender ! Message.TicketResponse(-1)
                }
            }
    
        case msg : Message.NewTurn =>
            this.turn = msg.turn
            this.ticketMap = (DEFAULT_STARTING_TICKET to maxAvailableTickets)
                .zip(List.fill(maxAvailableTickets)(DEFAULT_BUYER)).toMap
            this.log("New tickets are available...")
    
        case _ : Message.EndGame =>
            this.log("Game has come to an end. Closing Ticketing Service...")
            context.become({
                case _ : Message.Close => context.stop(self)
                case _ => this.desist
            })
        case _ : Message.Close => context.stop(self)
        case _ => this.desist
    }
}

object TicketDispenser{
    type Ticket = Int
    val DEFAULT_STARTING_TICKET : Ticket = 1
}