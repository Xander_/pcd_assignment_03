package pcd.ass03.ex1

import javax.swing.SwingUtilities

import akka.actor.{Actor, ActorRef, Props}

/**
  * Created by Xander_C on 09/05/2017.
  */
class Updater extends Actor{
    
    private var gui : ScalaGUI = _
    
    private var oracle : ActorRef = _
    
    override def receive = {
        //case msg : ScalaGUI => if (gui == null) gui = msg
        case msg : Message.Init =>
            gui = new ScalaGUI(self)
            oracle = context.actorOf(Props[Oracle], "Cassandra")
    
            gui.show("Guess the Number", 500, 200)
            gui.setResizable(false)
            gui.setVisible(true)
            
            context.become({
                case msg : Message.Update =>
                    SwingUtilities.invokeLater(() => {
                        gui.setNumberDisplay("Chosen Number is " + msg.number)
                        gui.setTurnDisplay("Turn n°" + msg.turn)
                    })
                case msg : Message.EndGame =>
                    SwingUtilities.invokeLater(() => {
                        gui.setWinnerDisplay("Winner is " + msg.winner)
                    })
    
                case _ : Message.Reset =>
                    oracle ! Message.Close()
                    SwingUtilities.invokeLater(() => {
                        gui.setNumberDisplay("")
                        gui.setTurnDisplay("")
                        gui.setWinnerDisplay("")
                    })
                case msg : Message.Start =>
                    oracle ! Message.Init(String.valueOf(Long.MaxValue), String.valueOf(msg.nPlayers))
                    oracle ! Message.NewTurn(0)
                case _ : Message.Close =>
                    oracle ! Message.Close()
                    SwingUtilities.invokeLater(() => {
                        gui.setNumberDisplay("")
                        gui.setTurnDisplay("")
                        gui.setWinnerDisplay("")
                    })
        
                    val fut = context.system.terminate()
                    this.log("Closing Application...")
                    fut.onComplete[Any](_ => System.exit(0))(scala.concurrent.ExecutionContext.Implicits.global)
    
                case _ => null
            })
        
        case _ => null
    }
    
    private def log(msg: String) = System.out.println("[" + self.path.name + "]:" + msg)
}
