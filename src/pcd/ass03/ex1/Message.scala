package pcd.ass03.ex1

/**
  * Created by Alessandro on 07/05/2017.
  */

trait Message{
    
    def isEmpty: Boolean
}

object Message{
    
    type Ticket = Int
    
    case class Init(args: String*) extends Message{
        override def isEmpty: Boolean = true
    }
    
    case class TicketRequest(sender: String) extends Message{
        override def isEmpty: Boolean = sender == null
    }
    
    case class TicketResponse(ticket: Ticket) extends Message{
        override def isEmpty: Boolean = ticket <= 0
    }
    
    case class NewTurn(turn : Int) extends Message{
        override def isEmpty: Boolean = true
    }
    
    case class EndGame(winner : String) extends Message{
        override def isEmpty: Boolean = winner == null
    }
    
    case class OracleResponse(private val comp : Long) extends Message{
        
        def isGreater: Boolean =  comp > 0 //guess > real
        
        def isLesser: Boolean = comp < 0 // guess < real
    
        override def isEmpty: Boolean = comp == 0
    }
    
    case class Guess(sender: String, guess: Long, ticket: Ticket) extends Message {
        override def isEmpty: Boolean = sender == null || guess < 0 || ticket <= 0
    }
    
    case class Error(content : String) extends Message{
        override def isEmpty: Boolean = false
    }
    
    case class Log(content: String) extends Message {
        override def isEmpty = content != null
    }
    
    case class Update(number : Long, turn : Int) extends Message {
        override def isEmpty = number < 0 || turn < 0
    }
    
    case class Start(nPlayers : Int) extends Message{
        override def isEmpty = true
    }
    
    case class Reset() extends Message{
        override def isEmpty = true
    }
    
    case class Close() extends Message {
        override def isEmpty = true
    }
}