package pcd.ass03.ex1

import java.util.concurrent.ThreadLocalRandom

import akka.actor._

/**
  * Created by Alessandro on 07/05/2017.
  */
class Oracle extends AnotherActor{
    
    val DEFAULT_WINNER = "none"
    
    type Ticket = Int
    
    private var players : Int = _
    private var nextTicket : Ticket = _
    private var turn : Int = _
    
    protected def init(args : List[String]) = args match {
        case seed :: nTickets :: _ =>
            this.ticketeer = "Ticketeer"
            
            players = nTickets.toInt
            turn = 0
            nextTicket = TicketDispenser.DEFAULT_STARTING_TICKET
            
            this.guess = ThreadLocalRandom.current.nextLong(0L, Math.abs(seed.toLong))
            
            this.log("Activating " + ticketeer +" services...")
            context.actorOf(Props[TicketDispenser], ticketeer) ! Message.Init(nTickets)
            
            this.log("Looking for " + nTickets + " Players...")
            (1 to players).foreach(id => {
                val pl = Player.PLAYER_NAME_OFFSET + id
                context.actorOf(Props[Player], pl) ! Message.Init(seed, ticketeer)
            })
        
        case _ => throw new IllegalArgumentException("Illegals Init arguments for " + this.getClass)
    }
    
    protected val receptive : Actor.Receive = {
        case msg: Message.Guess =>
            if (msg.ticket == nextTicket) {
                if (guess == msg.guess) {
                    this.log("Game is Over, " + msg.sender + " with ticket #" + msg.ticket + " has WON...")
                    this.broadcast(Message.EndGame(msg.sender))(_ => true)
                    
//                    this.sibling("Updater").get ! Message.Update(this.guess, turn)
//                    this.sibling("Updater").get ! Message.EndGame(msg.sender)
                    this.parent ! Message.Update(this.guess, turn)
                    this.parent ! Message.EndGame(msg.sender)
                    
                    context.unbecome()
                    
                    context.become({
                        case _ : Message.Close =>
                            this.broadcast(Message.Close())(_ => true)
                            context.unbecome()
                        case _ => this.desist
                    }, true)
                
                } else {
                    this.log(msg.sender + " with ticket #" + msg.ticket + " <= NOPE!")
                    this.sender ! Message.OracleResponse(msg.guess - this.guess)
    
                    if (nextTicket + 1 == players + 1) { // player +1 'cause I start from 1 :P
                        nextTicket = TicketDispenser.DEFAULT_STARTING_TICKET
                        this.log("End of turn n° " + turn + "! Come here folks!\n")
                        self ! Message.NewTurn(turn + 1)
                    } else nextTicket += 1
                    
                    this.unstashAll()
                }
            
            } else {
                // Rimetto il messaggio nella mia mailbox
                this.log("Saving unordered ticket #" + msg.ticket +
                    " of " + msg.sender + ", required #" + nextTicket + "...")
                
                this.stash()
            }
        case msg : Message.NewTurn =>
            turn = msg.turn
            this.child(ticketeer).get ! Message.NewTurn(turn)
            this.parent ! Message.Update(this.guess, turn)
//            this.sibling("Updater").get ! Message.Update(this.guess, turn)
            this.broadcast(Message.NewTurn(turn))(a => a.path.name != ticketeer)
        case _ : Message.Close =>
            context.unbecome()
            this.broadcast(Message.Close())(_ => true)
        case _ => this.desist
    }
}