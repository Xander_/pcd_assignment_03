package pcd.ass03.ex1

import java.util.concurrent.ThreadLocalRandom

/**
  * Created by Alessandro on 07/05/2017.
  */
class Player extends AnotherActor{
    
    private var offset : (Long, Long) = _
    
    protected def init(args: List[String]) = args match {
        case max :: ticketeerID :: _ =>
            this.offset = (0L, max.toLong)
            this.ticketeer = ticketeerID
            this.guess = mumbling(0L, null)
            this.log("Here I am!")
            
        case _ => throw new IllegalArgumentException("Illegals Init Arguments for " + this.getClass)
    }
    
    override protected val receptive = {
        
        case _ : Message.NewTurn =>
            this.log("Asking for a new ticket...")
            this.sibling(ticketeer).get ! Message.TicketRequest(name)
        
        case msg : Message.TicketResponse =>
            if (!msg.isEmpty) {
                this.log("I got ticket #" + msg.ticket +"!")
                this.log("I guess the number is..." + this.guess)
                this.parent ! Message.Guess(name, guess, msg.ticket)
            } else {
                this.log("No ticket for me :'(")
//                this.sibling(ticketeer).get ! Message.TicketRequest(name)
            }
    
        case msg : Message.OracleResponse =>
            guess = this.mumbling(guess, msg)
            this.sibling(ticketeer).get ! Message.TicketRequest(name)
    
        case msg : Message.EndGame =>
            if(msg.winner == name) log("Win!") else log("Sob...")
            context.become({
                case _ : Message.Close => context.stop(self)
                case _ => this.desist
            })
        case _ : Message.Close => context.stop(self)
        case _ => this.desist
    }
    
    private def mumbling(guess : Long, response : Message.OracleResponse) : Long =
        if (response != null && !response.isEmpty) {
            this.offset = if (response.isLesser) (guess, offset._2) else (offset._1, guess)
            
            if (offset._1 / 2 + offset._2 / 2 == guess) offset._1 / 2 + offset._2 / 2 + 1L else offset._1 / 2 + offset._2 / 2
        } else {
            // Genero il primo guess in maniera randomica finchè non ne trovo uno soddisfacente
            ThreadLocalRandom.current.nextLong(offset._1, offset._2)
        }
}

object Player{
    val PLAYER_NAME_OFFSET = "Actor_"
}