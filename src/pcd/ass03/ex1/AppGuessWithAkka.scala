package pcd.ass03.ex1

import akka.actor.{ActorSystem, Props}

/**
  * Created by Xander_C on 18/05/2017.
  */
object AppGuessWithAkka extends App{
    private val system = ActorSystem.create("System")
    system.actorOf(Props[Updater], "Updater") ! Message.Init()
}
