package pcd.ass03.ex1

import akka.actor.{Actor, ActorRef, ActorSelection, Stash}
import akka.event.Logging

/**
  * Created by Xander_C on 09/05/2017.
  */
abstract class BasicActor extends Actor with Stash {
    
    //private val logger = Logging.getLogger(this.context.system, this)
    
    def name: String = self.path.name
    
    def parent : ActorRef = this.context.parent
    
    def sibling(name : String) : Option[ActorSelection] =
        Option(context.actorSelection(this.context.parent.path / name))
    
    def child(name : String) : Option[ActorRef] = this.context.child(name)
    
    def children : Iterable[ActorRef] = this.context.children
    
    /**
      * Broadcast the given message to all its children
      * that satisfies the given filter predicate
      *
      * @param msg The message to broadcast
      * @param filter A function to filter unwanted listener
      */
    def broadcast(msg : Message)(filter : ActorRef => Boolean) =
        this.children.filter(filter).foreach(child => child ! msg)
    
    override def receive = resistive
    
    protected val receptive : Actor.Receive
    
    protected val resistive : Actor.Receive = {
        case msg : Message.Init =>
            try {
                this.init(msg.args.toList)
            } catch {
                case ex : Throwable => ex.printStackTrace()
            } finally {
                this.context.become(receptive, discardOld = true)
            }
        
        case _ => this.desist
    }
    
    protected def init(args : List[String])
    
    def desist = null
    
    def log(msg: String) = println("[" + name +"]: " + msg)
    
}

abstract class AnotherActor extends BasicActor {
    
    private var ticketDispenserName = ""
    
    private var value = -1L
    
    def ticketeer_=(ticketeer: String) = this.ticketDispenserName = ticketeer
    
    def ticketeer: String = this.ticketDispenserName
    
    def guess_=(guess: Long) = this.value = guess
    
    def guess: Long = this.value
}
