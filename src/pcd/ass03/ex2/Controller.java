package pcd.ass03.ex2;

import io.vertx.core.Vertx;

/**
 * Created by Xander_C on 16/05/2017.
 */
public class Controller {

    private final String root;
    private final String oName;
    private final String tName;
    private final String uName = "Ulisse";

    private Vertx vertx;
    private View view;

    public Controller(final String root, final String oName, final String tName) {

        this.root = root;
        this.oName = oName;
        this.tName = tName;

        vertx = Vertx.vertx();
    }

    void start(final int nPlayers){

        final Oracle oracle = new Oracle(root, oName);
        final TicketDispenser ticketeer = new TicketDispenser(root, tName);
        final Updater updater = new Updater(root, uName, view);

        vertx.deployVerticle(oracle);
        vertx.deployVerticle(ticketeer);
        vertx.deployVerticle(updater);

        final String oArg = String.join(";", String.valueOf(Long.MAX_VALUE), uName);
        final String tArg = String.join(";", oName);

        vertx.eventBus().send(root + "." + oName + ".init", oArg, init ->
                vertx.eventBus().send(root + "." + tName + ".init", tArg, subs ->
                        vertx.eventBus().send(root + "." + oName + ".add-ticketeer", tName)
                )
        );

        vertx.eventBus().send(root + "." + oName + ".add-updater", uName);

        for (int i = 1; i < nPlayers + 1; i++) {
            vertx.deployVerticle(new Player(root, "Player_" + i, oName, tName));
        }

        final String arg = String.join(";", String.valueOf(Long.MAX_VALUE));
        final String vNameSpace = root + ".players.init";

        log("Sending subscribes invitation to players...");

        sleep(20L);
        vertx.eventBus().publish(vNameSpace, arg);

        sleep(20L);

        vertx.eventBus().send(root + "." + oName +".new-turn", 1);
    }

    public void reset(){
        vertx.eventBus().send(root + "." + uName + ".reset", null);
        vertx.eventBus().publish(root + ".close", null);
    }

    public void exit(){
        this.vertx.close();
        System.exit(1);
    }

    public void attachGUI(View view) {

        this.view = view;
    }

    private static void log(final String content) {
        System.out.println("[System]: " + content);
    }

    private void sleep(final long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
