package pcd.ass03.ex2;

import io.vertx.core.AbstractVerticle;

import javax.swing.*;

/**
 *
 * Created by Xander_C on 16/05/2017.
 */
public class Updater extends AbstractVerticle{

    private final String root;
    private final String name;
    private View view;

    public Updater(final String root, final String name, final View view) {

        this.root = root;
        this.name = name;
        this.view = view;
    }

    @Override
    public void start() throws Exception {

        vertx.eventBus().consumer(root + "." + name + ".GUI-updates")
                .handler(msg -> {
                    final String[] m = Utils.parse((String) msg.body());
                    SwingUtilities.invokeLater(() -> {
                        view.setNumberDisplay("Number is " + m[0]);
                        view.setTurnDisplay("Turn is " + m[1]);
                    });
                });

        vertx.eventBus().consumer(root + "." + name + ".end-game")
                .handler(msg -> {
                    final String[] m = Utils.parse((String) msg.body());
                    SwingUtilities.invokeLater(() -> {
                        view.setWinnerDisplay("Winner is " + m[0]);
                    });
                });

        vertx.eventBus().consumer(root + "." + name + ".reset")
                .handler(msg -> {
                    SwingUtilities.invokeLater(() ->{
                        view.setNumberDisplay("");
                        view.setTurnDisplay("");
                        view.setWinnerDisplay("");
                    });

                    vertx.undeploy(this.deploymentID());
                });
    }
}
