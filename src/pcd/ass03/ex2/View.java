package pcd.ass03.ex2;

import javax.swing.*;
import java.awt.*;

import static java.lang.Integer.MAX_VALUE;

/**
 * A simple class to manage a GUI
 *
 * Created by Alessandro on 07/05/2017.
 */
public class View extends JFrame{

    private final Controller controller;
    private final JLabel turn = new JLabel("");
    private final JLabel winner = new JLabel("");
    private final JLabel magic = new JLabel("");
    private final JButton start = new JButton("Start");

    public View(final String title, final Controller c, final int w, final int h){

        this.controller = c;
        this.setSize(w, h);
        this.setTitle(title);
        this.setLayout(new BorderLayout(10, 10));

        this.controller.attachGUI(this);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        final JPanel north = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        final JLabel tickets = new JLabel("Tickets");
        final JSpinner spinner = new JSpinner(new SpinnerNumberModel(3, 1, MAX_VALUE, 1));

        north.add(tickets);
        north.add(spinner);

        final JPanel main = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        final JButton start = new JButton("Start");
        final JButton reset = new JButton("Reset");
        final JButton exit = new JButton("Exit");

        start.addActionListener(e -> {
            controller.start((Integer) spinner.getValue());
            start.setEnabled(false);
            reset.setEnabled(true);
        });

        reset.setEnabled(false);
        reset.addActionListener(e -> {
            controller.reset();
            start.setEnabled(true);
            reset.setEnabled(false);
        });

        exit.addActionListener(e -> controller.exit());

        main.add(start);
        main.add(reset);
        main.add(exit);

        final JPanel south = new JPanel((new FlowLayout(FlowLayout.CENTER, 10, 10)));
        south.add(magic);
        south.add(turn);
        south.add(winner);

        this.add(north, BorderLayout.NORTH);
        this.add(main, BorderLayout.CENTER);
        this.add(south, BorderLayout.SOUTH);

        this.setResizable(false);
        this.setVisible(true);
    }

    public void setNumberDisplay(final String value){
        this.magic.setText(value);
    }

    public void setWinnerDisplay(final String value){
        this.winner.setText(value);
    }

    public void setTurnDisplay(final String value){
        this.turn.setText(value);
    }

}
