package pcd.ass03.ex2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Xander_C on 12/05/2017.
 */
public class TicketDispenser extends AbstractVerticle{

    public static final String DEF_OWNER = "None";

    private final String root;
    private final String myName;
    private int maxTickets;
    private Map<Integer, String> ticketMap;

    public TicketDispenser(final String root, final String name){

        this.root = root;
        this.myName = name;
    }

    @Override
    public void start() throws Exception {

        final MessageConsumer<Object> cInit =
                this.vertx.eventBus().consumer(root + "." + myName + ".init")
                    .handler(msg -> {
                        maxTickets = 0;
                        ticketMap = Collections.emptyMap();

                        log("Ticketing service is now active... " + maxTickets + " tickets are available at most.");
                        msg.reply(myName);
                    });

        final MessageConsumer<Object> cAdd =
            this.vertx.eventBus().consumer(root + "." + myName + ".add-tickets")
                    .handler(msg -> {
                       this.maxTickets++;
                       log("Increased ticket pool... Now " + maxTickets + " tickets are available at most.");

                       ticketMap = IntStream.range(1, maxTickets+1).boxed()
                               .collect(Collectors.toMap(
                                       k -> k,
                                       k -> ticketMap.getOrDefault(k, DEF_OWNER)));
                    });

        final MessageConsumer<Object> cReq =
            this.vertx.eventBus().consumer(root + "." + myName +".ticket-requests")
                    .handler(msg -> {
                        final String sender = (String) msg.body();

                        if (ticketMap.values().stream()
                                .filter(v -> v.equals(DEF_OWNER))
                                .noneMatch(v -> v.equals(sender))){

                            final Optional<Integer> ticket = ticketMap.keySet().stream()
                                    .filter(k -> ticketMap.get(k).equals(DEF_OWNER))
                                    .min(Comparator.comparingInt(k -> k));

                            ticket.ifPresent(t -> {
                                vertx.eventBus().send(root + "." + sender + ".tickets", t.toString());

                                ticketMap = IntStream.range(1, maxTickets+1).boxed()
                                        .collect(Collectors.toMap(
                                                k -> k,
                                                k -> k.equals(t) ? sender :
                                                        ticketMap.getOrDefault(k, DEF_OWNER)));
                            });
                        }
                    });

        final MessageConsumer<Object> cTurn =
            this.vertx.eventBus().consumer(root + "." + myName + ".new-turn")
                    .handler(msg -> {
                        ticketMap = IntStream.range(1, maxTickets+1).boxed()
                                .collect(Collectors.toMap(k -> k, k -> DEF_OWNER));
                        log("Flushed ticket map...");
                    });

        final MessageConsumer<Object> cEnd =
            this.vertx.eventBus().consumer(root + ".end-game")
                    .handler(msg -> {
                        log("Ticket Service is now Closed...");
                        cAdd.unregister();
                        cInit.unregister();
                        cReq.unregister();
                        cTurn.unregister();
                        // How the fuck do I kill it?
                        this.ticketMap = Collections.emptyMap();
                        this.maxTickets = 0;
                    });

        final MessageConsumer<Object> cClose =
                vertx.eventBus().consumer(root + ".close")
                        .handler(msg -> {
                            vertx.undeploy(this.deploymentID());
                        });
    }

    private void log(final String content){
        System.out.println("[" + myName + "]: " + content);
    }
}
