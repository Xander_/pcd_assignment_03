package pcd.ass03.ex2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Xander_C on 11/05/2017.
 */
public class Oracle extends AbstractVerticle {

    private Long seed;
    private Long value;

    private final String root;

    private String myName;

    private String ticketeer;
    private String updater;
    private int nextTicket;
    private int turn;
    private boolean gameOver = false;

    private List<String> subscriptions;
    private List<Object> stash;

    public Oracle(final String root, final String name) {
        this.root = root;
        this.myName = name;
        this.subscriptions = new ArrayList<>();
    }

    @Override
    public void start() throws Exception {

        final MessageConsumer<Object> cInit =
            this.vertx.eventBus().consumer(root + "." + myName +".init")
                    .handler(msg -> {
                        log("Hello there, you sweety :*");

                        seed = Long.parseLong(Utils.parse((String) msg.body())[0]);
                        value = ThreadLocalRandom.current().nextLong(seed);
                        log("Magic number is " + value);

                        subscriptions = new ArrayList<>();
                        nextTicket = 1;
                        turn = 0;
                        gameOver = false;
                        stash = new ArrayList<>();

                        msg.reply(null);
                    });

        final MessageConsumer<Object> cSubs =
            this.vertx.eventBus().consumer(root + "." + myName + ".subscriptions")
                    .handler(msg -> {
                        this.subscribes((String) msg.body());
                        vertx.eventBus().send(root + "." + ticketeer + ".add-tickets", msg.body());
                        log("Subscribed " + msg.body() + " to the game...");
                        log("Players are " + subscriptions.size());
                    });

        final MessageConsumer<Object> cTicketeer =
            this.vertx.eventBus().consumer(root + "." + myName + ".add-ticketeer")
                    .handler(msg -> {
                        ticketeer = (String) msg.body();
                        log("Updated ticketeer is " + ticketeer +"...");
                    });

        final MessageConsumer<Object> cUpdater =
                vertx.eventBus().consumer(root + "." + myName + ".add-updater")
                        .handler(msg -> {
                            updater = (String) msg.body();
                            log("Updated updater is " + updater +"...");
                        });

        final MessageConsumer<Object> cTurn =
            this.vertx.eventBus().consumer(root + "." + myName + ".new-turn")
                    .handler(msg -> {
                        log("End of turn #" + turn + " -> Starting Turn #" + msg.body());
                        this.turn = (int) msg.body();

                        vertx.eventBus().send(root + "." + ticketeer + ".new-turn", turn);

                        vertx.eventBus().publish(root + ".players.new-turn", turn);

                        final String m = String.join(";", value.toString(),
                                String.valueOf(turn));

                        vertx.eventBus().send(root + "." + updater + ".GUI-updates", m);
                    });

        final MessageConsumer<Object> cGuess =
            this.vertx.eventBus().consumer(root + "." + myName + ".guesses")
                    .handler(msg -> {
                        if (!gameOver){
                            final Guess g = new Guess((String) msg.body());

                            if (g.ticket == nextTicket) {
                                if (value.equals(g.guess)) {

                                    gameOver = true;

                                    this.log("Game is Over, " + g.sender +
                                            " with ticket #" + g.ticket + " has WON...\n " +
                                            " -> The number is " + g.guess);

                                    subscriptions = new ArrayList<>();
                                    stash = new ArrayList<>();
                                    turn = 0;

                                    cInit.unregister();
                                    cSubs.unregister();
                                    cTicketeer.unregister();
                                    cTurn.unregister();
                                    cUpdater.unregister();

                                    final String namespace = root + ".end-game";
                                    vertx.eventBus().publish(namespace, g.sender);
                                    vertx.eventBus().send(root + "." + updater + ".end-game", g.sender);

                                } else {

                                    this.log(g.sender + " with ticket #" + g.ticket + " <= NOPE!");
                                    final String senderNameSpace = root + "." + g.sender +".guess-responses";
                                    vertx.eventBus().send(senderNameSpace, g.guess < value);

                                    if (nextTicket + 1 == subscriptions.size() + 1) {
                                        nextTicket = 1;

                                        this.stash = new ArrayList<>();
                                        this.log("End of turn n° " + turn + "! Come here folks!\n");

                                        vertx.eventBus().send(root + "." + myName + ".new-turn",
                                                turn + 1);

                                    } else {
                                        nextTicket++;
                                        this.unstashAll();
                                    }
                                }
                            } else {
                                // Rimetto il messaggio nella mia mailbox
                                this.log("Saving unordered ticket #" + g.ticket +
                                        " of " + g.sender + ", required #" + nextTicket + "...");
                                this.stash(msg.body());
                            }
                        }
                    });

        final MessageConsumer<Object> cClose =
                vertx.eventBus().consumer(root + ".close")
                        .handler(msg -> {
                            vertx.undeploy(this.deploymentID());
                        });
    }

    public List<String> subscriptions() {
        return Collections.unmodifiableList(subscriptions);
    }

    public void subscribes(String id){
        this.subscriptions.add(id);
    }

    public String name() {
        return myName;
    }

    public void log(final String content){
        System.out.println("[" + myName + "]: " + content);
    }

    private void unstashAll() {
        for (Object m : stash){
            vertx.eventBus().send(root + "." + myName + ".guesses", m);
        }

        stash = new ArrayList<>();
    }

    private void stash(Object msg) {
        stash.add(msg);
    }

    private void sleep(Long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class Guess {

        private final String sender;
        private final int ticket;
        private final Long guess;

        public Guess(final String blob){
            final String[] g = Utils.parse(blob);
            sender = g[0];
            ticket = Integer.parseInt(g[2]);
            guess = Long.parseLong(g[1]);
        }
    }
}