package pcd.ass03.ex2;

/**
 * Created by Alessandro on 07/05/2017.
 */
public class TestGuessWithVertX {

    private static String root = "game";
    private static String oName = "Cassandra";
    private static String tName = "Eucmene";
    private static Integer players = 7;

    public static void main(String[] args) {
        new View("Guess the Number", new Controller(root, oName, tName), 500, 250);
    }
}
