package pcd.ass03.ex2;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.MessageConsumer;
import javafx.util.Pair;

import java.util.concurrent.ThreadLocalRandom;

/**
 * LalalaFuckingLand
 * Created by Xander_C on 12/05/2017.
 */
public class Player extends AbstractVerticle {

    private final String root;
    private final String myName;
    private String oracle;
    private String ticketeer;

    private Long guess = -1L;
    private Pair<Long, Long> offset = new Pair<>(0L, 0L);

    public Player(final String root, final String name,
                  final String oracle, final String ticketeer) {

        this.root = root;
        this.myName = name;
        this.oracle = oracle;
        this.ticketeer = ticketeer;

    }

    @Override
    public void start() throws Exception {

        final MessageConsumer<Object> cInit =
            this.vertx.eventBus().consumer(root + ".players.init")
                    .handler(msg -> {
                        log("Here I'am, ma'am! o/");

                        final String[] m = Utils.parse((String) msg.body());
                        offset = new Pair<>(0L, Long.parseLong(m[0]));
                        guess = mumbling(null, null);
                        log("First guess is " + guess.toString());
                        vertx.eventBus().send(root + "." + oracle + ".subscriptions", myName);

                        log("Subscription sent...");
                    });

        final MessageConsumer<Object> cTurn =
            this.vertx.eventBus().consumer(root + ".players.new-turn")
                    .handler(msg -> {
                        final String namespace = root + "." + ticketeer + ".ticket-requests";
                        this.vertx.eventBus().send(namespace, myName);
                    });

        final MessageConsumer<Object> cTickets =
            this.vertx.eventBus().consumer(root + "." + myName + ".tickets")
                    .handler(msg -> {
                        final Integer ticket = Integer.parseInt(Utils.parse((String) msg.body())[0]);
                        if (ticket > 0) {
                            log("I got ticket #" + ticket);
                            final String namespace = root + "." + oracle + ".guesses";
                            this.vertx.eventBus().send(namespace, String.join(";",
                                    myName, guess.toString(), ticket.toString()));
                            log("Guess " + guess + " sent to " + oracle);
                        } else {
                            log("No ticket for me :'( ...");
                        }
                    });

        final MessageConsumer<Object> cResp =
            this.vertx.eventBus().consumer(root + "." + myName + ".guess-responses")
                    .handler(msg -> guess = mumbling(guess, (boolean) msg.body()));

        final MessageConsumer<Object> cEnd =
            this.vertx.eventBus().consumer(root + ".end-game")
                    .handler(msg -> {
                        cInit.unregister();
                        cResp.unregister();
                        cTickets.unregister();
                        cTurn.unregister();
                        final String winner = Utils.parse((String) msg.body())[0];
                        log(winner.equals(myName) ? "Win!" : "Sob...");
                    });

        final MessageConsumer<Object> cClose =
                vertx.eventBus().consumer(root + ".close")
                .handler(msg -> {
                    vertx.undeploy(this.deploymentID());
                });
    }

    public Long mumbling(final Long guess, final Boolean isLesser) {
        if (guess != null && isLesser != null) {
            this.offset = isLesser ? new Pair<>(guess, offset.getValue()) :
                    new Pair<>(offset.getKey(), guess);

            return offset.getKey() / 2 + offset.getValue() / 2 == guess ?
                    offset.getKey() / 2 + offset.getValue() / 2 + 1L :
                    offset.getKey() / 2 + offset.getValue() / 2;
        } else {
            // Genero il primo guess in maniera randomica finchè non ne trovo uno soddisfacente
            return ThreadLocalRandom.current().nextLong(offset.getKey(), offset.getValue());
        }
    }

    public String name() {
        return myName;
    }

    private void log(final String content) {
        System.out.println("[" + myName + "]: " + content);
    }
}